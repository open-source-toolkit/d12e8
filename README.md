# Flowable 6.8.0 资源文件下载

## 简介
本仓库提供了一个包含 Flowable 6.8.0 版本的资源文件下载，其中包括了 FlowableUI 设计器的部署包。该资源文件是从官方网站下载的，确保准确无误。资源文件中包含了可直接部署到 Tomcat 运行的 WAR 包。

## 资源内容
- **Flowable 6.8.0 版本**：包含 FlowableUI 设计器的完整部署包。
- **WAR 包**：可以直接部署到 Tomcat 服务器上运行。

## 使用说明
1. **下载资源文件**：
   - 点击仓库中的资源文件进行下载。

2. **部署到 Tomcat**：
   - 将下载的 WAR 包放置到 Tomcat 的 `webapps` 目录下。
   - 启动 Tomcat 服务器，WAR 包将自动解压并部署。

3. **访问 FlowableUI**：
   - 打开浏览器，访问 `http://localhost:8080/flowable-ui`（假设 Tomcat 运行在默认端口 8080）。

## 注意事项
- 确保 Tomcat 服务器已正确安装并配置。
- 如果需要修改配置，请参考 Flowable 官方文档进行相应调整。

## 相关链接
- [Flowable 官方网站](https://www.flowable.org/)
- [Flowable GitHub 仓库](https://github.com/flowable/flowable-engine)

## 贡献
欢迎提交问题和建议，帮助改进本资源文件。

## 许可证
本资源文件遵循 Flowable 的开源许可证。详情请参考官方文档。